package hr.fer.zvne.planinari.model

data class RouteResponse(val type: String, val features: List<Feature>)
