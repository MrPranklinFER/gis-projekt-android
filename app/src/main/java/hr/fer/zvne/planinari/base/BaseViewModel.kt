package hr.fer.zvne.planinari.base

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

abstract class BaseViewModel(savedStateHandle: SavedStateHandle): ViewModel() {
    protected val disposable = CompositeDisposable()

    private val logTag = this::class.java.simpleName

    override fun onCleared() {
        super.onCleared()

        Timber.tag(logTag).d("onCleared")

        disposable.clear()
    }
}