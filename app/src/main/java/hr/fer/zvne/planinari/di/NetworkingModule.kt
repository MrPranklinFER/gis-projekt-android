package hr.fer.zvne.planinari.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hr.fer.zvne.planinari.BuildConfig
import hr.fer.zvne.planinari.networking.PlaninariApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkingModule {
    companion object {
        const val OK_HTTP_LOG_TAG = "OkHttp"
        const val TIMEOUT_REQUEST: Long = 30
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(gson: Gson): HttpLoggingInterceptor {
        return HttpLoggingInterceptor { message ->
            if (message.startsWith("{") || message.startsWith("[")) {
                try {
                    Timber.tag(OK_HTTP_LOG_TAG).v(gson.toJson(JsonParser.parseString(message)))
                } catch (t: Throwable) {
                    Timber.tag(OK_HTTP_LOG_TAG).w(t)
                    Timber.tag(OK_HTTP_LOG_TAG).v(message)
                }
            } else {
                Timber.tag(OK_HTTP_LOG_TAG).v(message)
            }
        }.apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    @Provides
    @Singleton
    fun provideOkHttpClientBuilder(
        logger: HttpLoggingInterceptor
    ): OkHttpClient {

        return OkHttpClient.Builder()
            .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
            .addNetworkInterceptor(logger)
            .build()
    }

    @Singleton
    @Provides
    fun provideGson(): Gson = GsonBuilder().setPrettyPrinting().create()

    @Singleton
    @Provides
    fun provideRetrofitService(
        client: OkHttpClient,
        gson: Gson
    ): PlaninariApi {
        return Retrofit.Builder()
            .client(client)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BuildConfig.BASE_URL).build().create(PlaninariApi::class.java)
    }
}