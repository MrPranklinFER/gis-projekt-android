package hr.fer.zvne.planinari.model

/**
 * @param coordinates list of lon, lat, altitude
 */
data class Feature(val type: String, val geometry: Geometry)