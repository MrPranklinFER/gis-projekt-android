package hr.fer.zvne.planinari.model

data class NavigationPoint(val lat: Double, val lon: Double)