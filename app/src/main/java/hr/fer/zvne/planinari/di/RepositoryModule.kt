package hr.fer.zvne.planinari.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import hr.fer.zvne.planinari.networking.repository.IPlaninariRepository
import hr.fer.zvne.planinari.networking.repository.PlaninariRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindPlaninariRepository(planinariRepository: PlaninariRepository): IPlaninariRepository
}