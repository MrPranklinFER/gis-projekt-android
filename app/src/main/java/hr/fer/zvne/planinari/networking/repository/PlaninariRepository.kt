package hr.fer.zvne.planinari.networking.repository

import hr.fer.zvne.planinari.model.NavigationPoint
import hr.fer.zvne.planinari.model.RouteRequest
import hr.fer.zvne.planinari.networking.PlaninariApi
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject


class PlaninariRepository @Inject constructor(private val api: PlaninariApi): IPlaninariRepository {

    override fun getRoute(points: List<NavigationPoint>): Single<String> {
        return api.getRoute(RouteRequest.fromNavigationPoints(points))
    }

    override suspend fun uploadPhoto(file: File) {
        val reqFile = file.asRequestBody("image/*".toMediaTypeOrNull())
        val body = MultipartBody.Part.createFormData("imageFile", file.name, reqFile)
        return api.uploadImage(body)
    }
}