package hr.fer.zvne.planinari.view

import dagger.hilt.android.AndroidEntryPoint
import hr.fer.zvne.planinari.R
import hr.fer.zvne.planinari.base.BaseActivity
import hr.fer.zvne.planinari.databinding.ActivityMainBinding
import hr.fer.zvne.planinari.util.viewBinding

@AndroidEntryPoint
class MainActivity: BaseActivity(R.layout.activity_main) {

    private val binding by viewBinding(ActivityMainBinding::inflate)
}