package hr.fer.zvne.planinari.base

import android.os.Bundle
import android.os.PersistableBundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import timber.log.Timber

abstract class BaseActivity(@LayoutRes layout: Int) : AppCompatActivity(layout){

    private val logTag = this::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

        Timber.tag(logTag).d("onCreate")
    }

    override fun onResume() {
        super.onResume()

        Timber.tag(logTag).d("onResume")
    }

    override fun onPause() {
        super.onPause()

        Timber.tag(logTag).d("onPause")
    }

    override fun onDestroy() {
        super.onDestroy()

        Timber.tag(logTag).d("onDestroy")
    }
}