package hr.fer.zvne.planinari.base

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import timber.log.Timber
import kotlin.math.log

abstract class BaseFragment(@LayoutRes layout: Int): Fragment(layout){

    private val logTag = this::class.java.simpleName

    fun toast(message:String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    fun toast(@StringRes message: Int) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.tag(logTag).d("onCreate")
    }

    override fun onResume() {
        super.onResume()

        Timber.tag(logTag).d("onResume")
    }

    override fun onPause() {
        super.onPause()

        Timber.tag(logTag).d("onPause")
    }

    override fun onDestroy() {
        super.onDestroy()

        Timber.tag(logTag).d("onDestroy")
    }
}