package hr.fer.zvne.planinari.worker

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.hilt.work.HiltWorker
import androidx.work.*
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import hr.fer.zvne.planinari.R
import hr.fer.zvne.planinari.networking.repository.IPlaninariRepository
import hr.fer.zvne.planinari.util.ImageCaptureUtil
import timber.log.Timber
import java.util.concurrent.TimeUnit

@HiltWorker
class UploadWorker @AssistedInject constructor(
    @Assisted private val appContext: Context,
    @Assisted workerParams: WorkerParameters,
    private val repo: IPlaninariRepository
) : CoroutineWorker(appContext, workerParams) {

    companion object {
        private const val NOTIFICATION_IN_PROGRESS_ID = 32
        private const val NOTIFICATION_DONE_ID = 42
        private const val NOTIFICATION_FAILED_ID = 52

        private const val WORK_NAME = "ImageUploadWork"

        fun startPeriodic(context: Context) {
            val uploadWorkRequest: PeriodicWorkRequest =
                PeriodicWorkRequestBuilder<UploadWorker>(30L, TimeUnit.MINUTES)
                    .build()

            WorkManager
                .getInstance(context)
                .enqueueUniquePeriodicWork(
                    WORK_NAME,
                    ExistingPeriodicWorkPolicy.REPLACE,
                    uploadWorkRequest
                )

            Timber.tag("UPLOAD").d("Enqueued periodic unique work $WORK_NAME")
        }

        fun startSingle(context: Context) {
            val uploadWorkRequest: OneTimeWorkRequest =
                OneTimeWorkRequestBuilder<UploadWorker>()
                    .build()

            WorkManager
                .getInstance(context)
                .enqueueUniqueWork(WORK_NAME, ExistingWorkPolicy.REPLACE, uploadWorkRequest)

            Timber.tag("UPLOAD").d("Enqueued unique work $WORK_NAME")
        }

        fun cancelAll(context: Context) {
            WorkManager
                .getInstance(context)
                .cancelUniqueWork(WORK_NAME)

            Timber.tag("UPLOAD").d("Canceled all unique work $WORK_NAME")
        }
    }

    private lateinit var builder: NotificationCompat.Builder

    private val totalPhotos by lazy { ImageCaptureUtil.getImageCount(appContext) }

    override suspend fun doWork(): Result {
        Timber.tag("UPLOAD").d("doWork")

        setForeground(createForegroundInfo())
        return if (ImageCaptureUtil.getImageCount(appContext) == 0) {
            Timber.tag("UPLOAD").d("No images to upload")
            cancelAll(appContext)
            Result.success()
        } else {
            Timber.tag("UPLOAD").d("Starting upload")
            startUpload()
        }
    }

    private fun createForegroundInfo(): ForegroundInfo {
        val channelId = applicationContext.getString(R.string.channel_id_foreground_service)
        val title = applicationContext.getString(R.string.image_sync_in_progress)

        // Create a Notification channel if necessary
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel()
        }

        builder = NotificationCompat.Builder(applicationContext, channelId)
            .setContentTitle(title)
            .setTicker(title)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setOngoing(true)

        val notification = builder.build()

        return ForegroundInfo(NOTIFICATION_IN_PROGRESS_ID, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        val name = appContext.getString(R.string.channel_name)
        val descriptionText = appContext.getString(R.string.channel_description)
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val mChannel = NotificationChannel(
            appContext.getString(R.string.channel_id_foreground_service),
            name,
            importance
        )
        mChannel.description = descriptionText
        val notificationManager =
            appContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(mChannel)
    }

    private fun showUploadDoneNotification() {
        val channelId = applicationContext.getString(R.string.channel_id_foreground_service)

        val notification = NotificationCompat.Builder(applicationContext, channelId)
            .setContentTitle(appContext.getString(R.string.image_upload_complete))
            .setContentText(
                appContext.getString(
                    R.string.successfully_uploaded_n_images,
                    totalPhotos
                )
            )
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .build()

        NotificationManagerCompat.from(appContext).apply {
            notify(NOTIFICATION_DONE_ID, notification)
            cancel(NOTIFICATION_FAILED_ID)
        }

        cancelAll(appContext)
    }

    private fun showUploadFailedNotification(remaining: Int) {
        val channelId = applicationContext.getString(R.string.channel_id_foreground_service)

        val notification = NotificationCompat.Builder(applicationContext, channelId)
            .setContentTitle(appContext.getString(R.string.image_upload_failed))
            .setContentText(
                appContext.getString(
                    R.string.failed_to_upload_n_of_m_images,
                    remaining,
                    totalPhotos
                )
            )
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .build()

        NotificationManagerCompat.from(appContext).notify(NOTIFICATION_FAILED_ID, notification)
    }


    private suspend fun startUpload(): Result {
        return ImageCaptureUtil.getLastImage(appContext)?.let { imageFile ->
            try {
                Timber.tag("UPLOAD").d("start upload")
                updateProgress(
                    totalPhotos,
                    totalPhotos - ImageCaptureUtil.getImageCount(appContext)
                )

                repo.uploadPhoto(imageFile)
                Timber.tag("UPLOAD").d("uploaded")

                ImageCaptureUtil.removeLastImage(appContext)
                Timber.tag("UPLOAD").d("Removed last image")

                val remainingAfter = ImageCaptureUtil.getImageCount(appContext)

                if (remainingAfter > 0) {
                    Timber.tag("UPLOAD").d("There are $remainingAfter images left to upload")

                    startUpload()
                } else {
                    Timber.tag("UPLOAD").d("Upload complete")

                    showUploadDoneNotification()

                    return Result.success()
                }
            } catch (t: Throwable) {
                Timber.tag("UPLOAD").e(t)
                showUploadFailedNotification(ImageCaptureUtil.getImageCount(appContext))
                return Result.failure()
            }
        } ?: Result.failure()
    }

    private fun updateProgress(max: Int, current: Int) {
        Timber.tag("UPLOAD").d("Upload progress: $current/$max")

        NotificationManagerCompat.from(appContext).apply {
            builder.setProgress(max, current, false)
            builder.setContentText("Upload progress: $current/$max")
            notify(NOTIFICATION_IN_PROGRESS_ID, builder.build())
        }
    }
}