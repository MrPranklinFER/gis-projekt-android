package hr.fer.zvne.planinari.networking.repository

import hr.fer.zvne.planinari.model.NavigationPoint
import io.reactivex.Single
import java.io.File

interface IPlaninariRepository {
    fun getRoute(points: List<NavigationPoint>): Single<String>
    suspend fun uploadPhoto(file: File)
}