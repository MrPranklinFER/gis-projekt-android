package hr.fer.zvne.planinari.viewModel

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.MultiPoint
import com.mapbox.geojson.Point
import dagger.hilt.android.lifecycle.HiltViewModel
import hr.fer.zvne.planinari.R
import hr.fer.zvne.planinari.base.BaseViewModel
import hr.fer.zvne.planinari.model.FeatureDetail
import hr.fer.zvne.planinari.model.FullFeatureData
import hr.fer.zvne.planinari.model.NavigationPoint
import hr.fer.zvne.planinari.networking.repository.IPlaninariRepository
import hr.fer.zvne.planinari.util.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MapViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val repo: IPlaninariRepository
) : BaseViewModel(savedStateHandle) {

    companion object {
        private const val imagesBaseUrl = "https://www.hps.hr/files/data/"
        private const val imagesBaseUrl2 = "https://www.hps.hr/karta/"
    }

    enum class Mode {
        NORMAL, CREATING_ROUTE, NAVIGATION
    }

    private var activeMode = Mode.NORMAL
        set(value) {
            field = value
            _viewState.value = value
        }

    private val tempNavigationWaypoints = arrayListOf<NavigationPoint>()

    // to be fetched from backend
    private val _styleUri =
        MutableLiveData(Event("mapbox://styles/fb50596/cky1djhh7hfds15qzfl6zflba")) // HPD
    val styleUri: LiveData<Event<String>>
        get() = _styleUri

    private val _viewState = MutableLiveData(activeMode)
    val viewState: LiveData<Mode>
        get() = _viewState

    private val _navigationPointsInPlanning = MutableLiveData<List<NavigationPoint>>()
    val navigationPointsInPlanning: LiveData<List<NavigationPoint>>
        get() = _navigationPointsInPlanning

    private val _navigationWaypoints = MutableLiveData<Pair<FeatureCollection, FeatureCollection>>()
    val navigationWaypoints: LiveData<Pair<FeatureCollection, FeatureCollection>>
        get() = _navigationWaypoints

    private val _showMarkerData = MutableLiveData<Event<FullFeatureData>>()
    val showMarkerData: LiveData<Event<FullFeatureData>>
        get() = _showMarkerData

    private val _routeLoading = MutableLiveData<Event<Boolean>>()
    val routeLoading: LiveData<Event<Boolean>>
        get() = _routeLoading

    private val _errorLoadingRoute = MutableLiveData<Event<String>>()
    val errorLoadingRoute: LiveData<Event<String>>
        get() = _errorLoadingRoute

    fun onNavigationClicked() {
        if (activeMode == Mode.NORMAL) {
            activeMode = Mode.CREATING_ROUTE
            _navigationWaypoints.value = Pair(
                FeatureCollection.fromFeatures(arrayListOf()),
                FeatureCollection.fromFeatures(arrayListOf())
            )
        }
    }

    fun onRouteDoneClicked(startLocation: Location?) {
        activeMode = Mode.NORMAL //Mode.NAVIGATION

        startLocation?.let {
            tempNavigationWaypoints.add(
                0,
                NavigationPoint(it.latitude, it.longitude)
            )
        }

        disposable.add(repo.getRoute(tempNavigationWaypoints)
            .subscribeOn(Schedulers.io())
            .map(FeatureCollection::fromJson)
            .map { featureCollection ->
                val multiPoint = MultiPoint.fromLngLats(tempNavigationWaypoints.map {
                    Point.fromLngLat(
                        it.lon,
                        it.lat
                    )
                })

                Pair(
                    featureCollection,
                    FeatureCollection.fromFeature(Feature.fromGeometry(multiPoint))
                )
            }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnEvent { _, _ -> _routeLoading.value = Event(false) }
            .doOnError { Timber.w(it) }
            .subscribe({
                Timber.d(it.toString())
                _navigationWaypoints.value = it

                tempNavigationWaypoints.clear()
                _navigationPointsInPlanning.value = arrayListOf()
            }, {
                _errorLoadingRoute.value = Event(it.message)

                tempNavigationWaypoints.clear()
                _navigationPointsInPlanning.value = arrayListOf()
            })
        )
    }

    fun onCancelRoutePlaningClicked() {
        activeMode = Mode.NORMAL

        tempNavigationWaypoints.clear()
        _navigationPointsInPlanning.value = arrayListOf()
    }

    fun onPointSelectedCreatingRoute(lat: Double, lon: Double) {
        if (activeMode == Mode.CREATING_ROUTE) {
            tempNavigationWaypoints.add(NavigationPoint(lat, lon))
            _navigationPointsInPlanning.value = tempNavigationWaypoints
        } else {
            Timber.e("Incorrect mode: $activeMode")
        }
    }

    fun onFeatureSelectedNormal(lat: Double, lon: Double, feature: Feature) {
        val detailsList = arrayListOf<FeatureDetail>()

        with(feature) {
            if (hasNonNullValueForProperty("type") && getStringProperty("type").isNotBlank())
                detailsList.add(FeatureDetail(R.string.type_label, getStringProperty("type")))
            if (hasNonNullValueForProperty("tours") && getStringProperty("tours").isNotBlank() && getStringProperty(
                    "tours"
                ) != "[]"
            )
                detailsList.add(
                    FeatureDetail(
                        R.string.hiking_route_label,
                        getStringProperty("tours").removePrefix("[").removeSuffix("]").split(",")
                            .joinToString("\n\n") { it.replace("\"", "") }
                    )
                )
            if (hasNonNullValueForProperty("url") && getStringProperty("url").isNotBlank())
                detailsList.add(FeatureDetail(R.string.url_label, getStringProperty("url")))

            detailsList.add(FeatureDetail(R.string.latitude_label, lat.toString()))
            detailsList.add(FeatureDetail(R.string.longitude_label, lon.toString()))
        }

        val name =
            if (feature.hasNonNullValueForProperty("name") && feature.getStringProperty("name")
                    .isNotBlank()
            ) feature.getStringProperty("name") else "N/A"

        val imageUrl =
            if (feature.hasNonNullValueForProperty("imageUrl") && feature.getStringProperty("imageUrl")
                    .isNotBlank()
            ) {
                val urlEnd = feature.getStringProperty("imageUrl")

                if (urlEnd.startsWith("foto/")) imagesBaseUrl2 + urlEnd else imagesBaseUrl + urlEnd
            } else {
                null
            }

        showPopup(FullFeatureData(feature.id().toString(), name, detailsList, imageUrl, lat, lon))
    }

    private fun showPopup(fullFeatureData: FullFeatureData) {
        _showMarkerData.value = Event(fullFeatureData)
    }

    private fun navigationDone() {
        activeMode = Mode.NORMAL
        _navigationWaypoints.value = Pair(
            FeatureCollection.fromFeatures(arrayListOf()),
            FeatureCollection.fromFeatures(arrayListOf())
        )
    }

    fun onExitNavigation() {
        navigationDone()
    }
}