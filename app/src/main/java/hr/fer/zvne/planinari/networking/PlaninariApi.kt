package hr.fer.zvne.planinari.networking

import hr.fer.zvne.planinari.model.RouteRequest
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface PlaninariApi {

    @POST("/route")
    fun getRoute(@Body routeRequest: RouteRequest): Single<String>

    @Multipart
    @POST("/image")
    suspend fun uploadImage(@Part image: MultipartBody.Part)
}