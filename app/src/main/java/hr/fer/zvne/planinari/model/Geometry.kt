package hr.fer.zvne.planinari.model

import java.nio.DoubleBuffer

/**
 * @param coordinates list of lon, lat, altitude
 */
data class Geometry(val type: String, val coordinates: List<List<Double>>)