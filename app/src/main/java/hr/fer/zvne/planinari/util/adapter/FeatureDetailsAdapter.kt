package hr.fer.zvne.planinari.util.adapter

import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.fer.zvne.planinari.databinding.ItemFeatureDetailBinding
import hr.fer.zvne.planinari.model.FeatureDetail

class FeatureDetailsAdapter(private val items: List<FeatureDetail>) :
    RecyclerView.Adapter<FeatureDetailsAdapter.ViewHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemFeatureDetailBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    class ViewHolder(private val binding: ItemFeatureDetailBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: FeatureDetail) {
            binding.textKey.text = binding.root.context.getString(item.key)
            binding.textValue.text = item.value
        }
    }
}