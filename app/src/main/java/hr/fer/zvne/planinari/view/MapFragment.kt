package hr.fer.zvne.planinari.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.drawable.VectorDrawable
import android.location.Location
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.LocationServices
import com.mapbox.bindgen.Expected
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.maps.*
import com.mapbox.maps.extension.style.expressions.dsl.generated.interpolate
import com.mapbox.maps.extension.style.layers.generated.circleLayer
import com.mapbox.maps.extension.style.layers.generated.lineLayer
import com.mapbox.maps.extension.style.layers.getLayer
import com.mapbox.maps.extension.style.layers.properties.generated.IconAnchor
import com.mapbox.maps.extension.style.layers.properties.generated.LineCap
import com.mapbox.maps.extension.style.layers.properties.generated.LineJoin
import com.mapbox.maps.extension.style.layers.properties.generated.Visibility
import com.mapbox.maps.extension.style.sources.generated.geoJsonSource
import com.mapbox.maps.extension.style.style
import com.mapbox.maps.plugin.LocationPuck2D
import com.mapbox.maps.plugin.animation.MapAnimationOptions
import com.mapbox.maps.plugin.animation.camera
import com.mapbox.maps.plugin.annotation.AnnotationConfig
import com.mapbox.maps.plugin.annotation.annotations
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationManager
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationOptions
import com.mapbox.maps.plugin.annotation.generated.createPointAnnotationManager
import com.mapbox.maps.plugin.gestures.OnMapClickListener
import com.mapbox.maps.plugin.gestures.addOnMapClickListener
import com.mapbox.maps.plugin.gestures.removeOnMapClickListener
import com.mapbox.maps.plugin.locationcomponent.location
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import hr.fer.zvne.planinari.R
import hr.fer.zvne.planinari.base.BaseFragment
import hr.fer.zvne.planinari.databinding.FragmentMapBinding
import hr.fer.zvne.planinari.databinding.ItemFeatureMarkerBinding
import hr.fer.zvne.planinari.model.FullFeatureData
import hr.fer.zvne.planinari.model.NavigationPoint
import hr.fer.zvne.planinari.util.ImageCaptureUtil
import hr.fer.zvne.planinari.util.adapter.FeatureDetailsAdapter
import hr.fer.zvne.planinari.util.observeEvent
import hr.fer.zvne.planinari.util.viewBinding
import hr.fer.zvne.planinari.viewModel.MapViewModel
import hr.fer.zvne.planinari.worker.UploadWorker
import timber.log.Timber

@AndroidEntryPoint
class MapFragment : BaseFragment(R.layout.fragment_map) {

    companion object {
        private const val NAVIGATION_PLANNING_POINTS_LAYER_ID = "navigation_planning_points"
        private const val PEAKS_LAYER_ID = "hpd-markers"

        private const val RC_IMAGE_CAPTURE = 3254
    }

    private val binding by viewBinding(FragmentMapBinding::bind)

    private val vm: MapViewModel by viewModels()

    private fun initLocationComponent() {
        val locationComponentPlugin = binding.mapView.location
        locationComponentPlugin.updateSettings {
            this.enabled = true
            this.locationPuck = LocationPuck2D(
                bearingImage = AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.mapbox_user_puck_icon,
                ),
                shadowImage = AppCompatResources.getDrawable(
                    requireContext(),
                    R.drawable.mapbox_user_icon_shadow,
                ),
                scaleExpression = interpolate {
                    linear()
                    zoom()
                    stop {
                        literal(0.0)
                        literal(0.6)
                    }
                    stop {
                        literal(20.0)
                        literal(1.0)
                    }
                }.toJson()
            )
        }
    }

    private val navigationPlanningAnnotationManager: PointAnnotationManager by lazy {
        binding.mapView.annotations.createPointAnnotationManager(
            AnnotationConfig(layerId = NAVIGATION_PLANNING_POINTS_LAYER_ID)
        )
    }

    private val onCreatingRouteClickListener = OnMapClickListener { point ->
        vm.onPointSelectedCreatingRoute(point.latitude(), point.longitude())
        true
    }

    @OptIn(MapboxExperimental::class)
    private val onMapClickNormalMode = OnMapClickListener { point ->
        binding.mapView.getMapboxMap().queryRenderedFeatures(
            RenderedQueryGeometry(binding.mapView.getMapboxMap().pixelForCoordinate(point)),
            RenderedQueryOptions(
                listOf(
                    PEAKS_LAYER_ID
                ), null
            )
        ) {
            onFeatureClicked(it) { feature ->
                vm.onFeatureSelectedNormal(point.latitude(), point.longitude(), feature)
            }
        }
        true
    }

    private fun showFeaturePopup(view: View?, fullFeatureData: FullFeatureData) {

        val popupView: View =
            LayoutInflater.from(requireContext()).inflate(R.layout.item_feature_marker, null)

        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        val screenWidth = displayMetrics.widthPixels

        val popupWindow =
            PopupWindow(popupView, screenWidth - 60, LinearLayout.LayoutParams.WRAP_CONTENT, true)

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)

        val binding = ItemFeatureMarkerBinding.bind(popupView)

        binding.textName.text = fullFeatureData.name
        binding.rvDetails.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = FeatureDetailsAdapter(fullFeatureData.details)
        }

        fullFeatureData.imageUrl?.let {
            Picasso.Builder(requireContext()).listener { _, uri, exception ->
                binding.ivImage.visibility = View.GONE
                Timber.w(exception, uri.toString())
            }.build().load(it).into(binding.ivImage)
            binding.ivImage.visibility = View.VISIBLE
        } ?: kotlin.run {
            binding.ivImage.visibility = View.GONE
        }

        // dismiss the popup window when touched
        popupView.setOnTouchListener { _, _ ->
            popupWindow.dismiss()
            true
        }
    }

    private fun onFeatureClicked(
        expected: Expected<String, List<QueriedFeature>>,
        onFeatureClicked: (Feature) -> Unit
    ) {
        Timber.d("onFeatureClicked: ${expected.value}")

        if (expected.isValue && expected.value?.size!! > 0) {
            expected.value?.get(0)?.feature?.let { feature ->
                Timber.d("feature recognized: ${feature.getStringProperty("name")}")
                onFeatureClicked(feature)
            }
        }
    }

    private val locationPermissionRequestForUserLocation = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        when {
            permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                initLocationComponent()
            }
            else -> {
                Timber.w("no location granted")
            }
        }
    }

    @SuppressLint("MissingPermission")
    private val locationPermissionRequestForNavigationFromUser = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        when {
            permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                LocationServices.getFusedLocationProviderClient(requireContext()).lastLocation.addOnCompleteListener {
                    if (it.isSuccessful) {
                        vm.onRouteDoneClicked(it.result)
                    } else {
                        Timber.d(it.exception)
                        toast("Failed to acquire location")
                    }
                }
            }
            else -> {
                Timber.w("no location granted")
            }
        }
    }

    @SuppressLint("MissingPermission")
    private val locationPermissionRequestForImageMetadata = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        when {
            permissions.getOrDefault(Manifest.permission.ACCESS_FINE_LOCATION, false) -> {
                LocationServices.getFusedLocationProviderClient(requireContext()).lastLocation.addOnCompleteListener {
                    if (it.isSuccessful) {
                        ImageCaptureUtil.setLocationInfoForLastImage(requireContext(), it.result)
                    } else {
                        Timber.d(it.exception)
                        toast("Failed to save GPS data")
                    }
                    UploadWorker.startPeriodic(requireActivity().applicationContext)
                }
            }
            else -> {
                Timber.w("No location granted")
                UploadWorker.startPeriodic(requireActivity().applicationContext)
            }
        }
    }

    private val cameraReadWritePermissionRequest = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        val camera = permissions.getOrDefault(Manifest.permission.CAMERA, false)

        if (camera) {
            startActivityForResult(ImageCaptureUtil.getIntent(requireContext()), RC_IMAGE_CAPTURE)
        } else {
            toast("Missing camera permission")
        }
    }

    private fun moveCameraToLocation(location: Location) {
        Timber.w("update camera $location")

        val mapAnimationOptions = MapAnimationOptions.Builder().duration(1500L).build()
        binding.mapView.camera.easeTo(
            CameraOptions.Builder()
                .center(Point.fromLngLat(location.longitude, location.latitude))
                .zoom(15.0)
                .padding(EdgeInsets(500.0, 0.0, 0.0, 0.0))
                .build(),
            mapAnimationOptions
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        locationPermissionRequestForUserLocation.launch(
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        )

        initUI()
        observeLiveData()

        UploadWorker.startPeriodic(requireContext())
    }

    private fun initUI() {
        binding.btnNavigation.setOnClickListener {
            vm.onNavigationClicked()
        }

        binding.btnPlanFromMyLocation.setOnClickListener {
            locationPermissionRequestForNavigationFromUser.launch(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
        }

        binding.btnPlan.setOnClickListener {
            vm.onRouteDoneClicked(null)
        }

        binding.btnCancel.setOnClickListener {
            vm.onCancelRoutePlaningClicked()
        }

        binding.btnExitNavigation.setOnClickListener {
            vm.onExitNavigation()
        }

        binding.btnCamera.setOnClickListener {
            cameraReadWritePermissionRequest.launch(
                arrayOf(
                    Manifest.permission.CAMERA
                )
            )
        }

        binding.btnUpload.setOnClickListener {
            UploadWorker.startSingle(requireActivity().applicationContext)
        }

    }

    private fun clearWaypointsInPlanning() {
        Timber.w("clearWaypointsInPlanning")
        navigationPlanningAnnotationManager.deleteAll()
    }

    private fun setNavigationWaypointsInPlanning(waypoints: List<NavigationPoint>) {
        Timber.w("adding planning waypoint: $waypoints")

        clearWaypointsInPlanning()

        waypoints.forEach { navPoint ->
            val pointAnnotationOptions: PointAnnotationOptions = PointAnnotationOptions()
                .withPoint(Point.fromLngLat(navPoint.lon, navPoint.lat))
                .withIconImage(
                    (ResourcesCompat.getDrawable(
                        requireContext().resources,
                        R.drawable.ic_baseline_location_on_24,
                        null
                    ) as VectorDrawable).toBitmap()
                )
                .withIconAnchor(IconAnchor.BOTTOM)

            navigationPlanningAnnotationManager.create(pointAnnotationOptions)
        }
    }

    private fun setNavigationData(
        linesFeatureCollection: FeatureCollection,
        waypointsFeatureCollection: FeatureCollection
    ) {
        Timber.d("setNavigationData")
        removeNavigationData()
        binding.mapView.getMapboxMap().loadStyle(
            style(vm.styleUri.value!!.peekContent()!!) {
                +geoJsonSource("NAVIGATION_LINES") {
                    featureCollection(linesFeatureCollection)
                }
                +geoJsonSource("NAVIGATION_WAYPOINTS") {
                    featureCollection(waypointsFeatureCollection)
                }
                Timber.d("working")

                +lineLayer("navigationRoute", "NAVIGATION_LINES") {
                    lineCap(LineCap.ROUND)
                    lineJoin(LineJoin.ROUND)
                    lineOpacity(1.0)
                    lineWidth(3.0)
                    lineColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.navigation_line_color
                        )
                    )
                }

                +circleLayer("navigationWaypoints", "NAVIGATION_WAYPOINTS") {
                    circleColor(ContextCompat.getColor(requireContext(), R.color.purple_500))
                    circleOpacity(1.0)
                    circleRadius(5.0)
                }
            })
    }

    private fun removeNavigationData() {
        binding.mapView.getMapboxMap().getStyle()?.getLayer("navigationRoute")
            ?.visibility(Visibility.NONE)
    }

    @OptIn(MapboxExperimental::class)
    private fun observeLiveData() {
        vm.styleUri.observeEvent(viewLifecycleOwner) {
            binding.mapView.getMapboxMap().loadStyleUri(it)
        }

        vm.viewState.observe(viewLifecycleOwner) {
            if (it == MapViewModel.Mode.CREATING_ROUTE) {
                binding.mapView.getMapboxMap().addOnMapClickListener(onCreatingRouteClickListener)
            } else {
                binding.mapView.getMapboxMap()
                    .removeOnMapClickListener(onCreatingRouteClickListener)
            }

            when (it) {
                MapViewModel.Mode.CREATING_ROUTE -> {
                    binding.mapView.getMapboxMap()
                        .addOnMapClickListener(onCreatingRouteClickListener)
                    binding.mapView.getMapboxMap().removeOnMapClickListener(onMapClickNormalMode)
                }
                MapViewModel.Mode.NORMAL -> {
                    binding.mapView.getMapboxMap()
                        .removeOnMapClickListener(onCreatingRouteClickListener)
                    binding.mapView.getMapboxMap().addOnMapClickListener(onMapClickNormalMode)
                }
                else -> {
                    binding.mapView.getMapboxMap()
                        .removeOnMapClickListener(onCreatingRouteClickListener)
                    binding.mapView.getMapboxMap().removeOnMapClickListener(onMapClickNormalMode)
                }
            }

            when (it) {
                MapViewModel.Mode.CREATING_ROUTE -> {
                    binding.groupModeNormal.visibility = View.INVISIBLE
                    binding.groupModeCreatingRoute.visibility = View.VISIBLE
                    binding.groupModeNavigation.visibility = View.INVISIBLE
                }
                MapViewModel.Mode.NORMAL -> {
                    binding.groupModeNormal.visibility = View.VISIBLE
                    binding.groupModeCreatingRoute.visibility = View.INVISIBLE
                    binding.groupModeNavigation.visibility = View.INVISIBLE
                }
                MapViewModel.Mode.NAVIGATION -> {
                    binding.groupModeNormal.visibility = View.INVISIBLE
                    binding.groupModeCreatingRoute.visibility = View.INVISIBLE
                    binding.groupModeNavigation.visibility = View.VISIBLE
                }
                else -> {
                } // nothing
            }
        }

        vm.navigationWaypoints.observe(viewLifecycleOwner) {
            setNavigationData(it.first, it.second)
        }

        vm.navigationPointsInPlanning.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                setNavigationWaypointsInPlanning(it)
            } else {
                clearWaypointsInPlanning()
            }
        }

        vm.showMarkerData.observeEvent(viewLifecycleOwner) {
            Timber.w("showMarkerData event: $it")
            showFeaturePopup(binding.mapView, it)
        }

        vm.errorLoadingRoute.observeEvent(viewLifecycleOwner) {
            toast(it)
        }

        vm.routeLoading.observeEvent(viewLifecycleOwner) {
            if (it) {
                toast(R.string.please_wait)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        try {
            navigationPlanningAnnotationManager.onDestroy()
        } catch (exc: IllegalStateException) {
            // was never used
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            locationPermissionRequestForImageMetadata.launch(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
            toast("Image saved")
        } else {
            toast("Image not saved")
        }
    }
}