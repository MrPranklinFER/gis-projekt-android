package hr.fer.zvne.planinari.model

import androidx.annotation.StringRes

data class FeatureDetail(@StringRes val key: Int, val value: String)
