package hr.fer.zvne.planinari.model

data class FullFeatureData(
    val id: String,
    val name: String,
    val details: List<FeatureDetail>,
    val imageUrl: String?,
    val lat: Double,
    val lon: Double
)
