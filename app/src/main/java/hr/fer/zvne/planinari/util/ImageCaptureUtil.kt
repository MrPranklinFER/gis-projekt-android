package hr.fer.zvne.planinari.util

import android.content.Context
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.location.Location
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import timber.log.Timber
import java.io.File
import java.util.*

object ImageCaptureUtil {

    private const val IMAGE_DIRECTORY_NAME = "images"
    private const val DATE_FORMAT = "yyyyMMdd_HHmmss"
    private const val FILE_PROVIDER_AUTHORITY = "hr.fer.zvne.planinari.fileprovider"

    private val sdf by lazy { SimpleDateFormat(DATE_FORMAT, Locale.getDefault()) }

    fun getIntent(context: Context): Intent {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).also {
            it.putExtra(MediaStore.EXTRA_OUTPUT, getOutputFile(context).getUri(context))
        }

        return intent
    }

    fun setLocationInfoForLastImage(context: Context, location: Location) {
        getOutputDirectory(context).listFiles()?.last()?.let {
            it.setLocation(location)
            Timber.d("GPS info saved: $location")
        } ?: Timber.d("Failed to open photo for setting location data")
    }

    fun getLastImage(context: Context): File? {
        return try {
            val allImages = File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME
            ).listFiles()

            val last = if (!allImages.isNullOrEmpty()) allImages.last() else null
            Timber.d("Last image: ${last?.absolutePath}")
            last
        } catch (t: Throwable) {
            Timber.e(t)
            null
        }
    }

    fun getImageCount(context: Context): Int {
        val count = File(
            context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            IMAGE_DIRECTORY_NAME
        ).listFiles()?.size ?: 0
        Timber.d("imageCount = $count")
        return count
    }

    fun removeLastImage(context: Context) {
        val allImages = File(
            context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            IMAGE_DIRECTORY_NAME
        ).listFiles()

        if (!allImages.isNullOrEmpty()) {
            allImages.last().delete()

            val remaining = File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME
            ).listFiles()
            Timber.d("Deleted last image, remaining: ${remaining?.size}")

            remaining?.forEach {
                Timber.tag("UPLOAD").d("------ ${it.absolutePath}")
            }
        }
    }

    private fun File.setLocation(location: Location) {
        try {
            ExifInterface(this).also {
                it.setGpsInfo(location)
                it.saveAttributes()
            }
        } catch (t: Throwable){
            Timber.e(t, "Failed to set GPS data")
        }
    }

    private fun File.getUri(context: Context): Uri {
        return FileProvider.getUriForFile(context, FILE_PROVIDER_AUTHORITY, this)
    }

    private fun getOutputDirectory(context: Context): File {
        val mediaStorageDir = File(
            context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            IMAGE_DIRECTORY_NAME
        )

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Timber.d("Folder $IMAGE_DIRECTORY_NAME exists")
            }
        }

        Timber.d("Photos dir: ${mediaStorageDir.absolutePath}")

        mediaStorageDir.listFiles()?.forEach {
            Timber.d("---- ${it.canonicalPath}")
        }

        return mediaStorageDir
    }

    private fun getOutputFile(context: Context): File {
        val file =
            File("${getOutputDirectory(context).path}${File.separator}IMG_${sdf.format(Date())}.jpg")
        Timber.d("Output file: ${file.absolutePath}")
        return file
    }
}