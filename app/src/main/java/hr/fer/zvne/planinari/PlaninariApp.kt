package hr.fer.zvne.planinari

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import javax.inject.Inject

@HiltAndroidApp
class PlaninariApp : Application(), Configuration.Provider {

    init {
        instance = this
    }

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    companion object {
        private var instance: PlaninariApp? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.BUILD_TYPE.contains("release", ignoreCase = true)) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun getWorkManagerConfiguration(): Configuration {
        Timber.tag("UPLOAD").d("getWorkManagerConfiguration")
        return Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .setMinimumLoggingLevel(Log.VERBOSE)
            .build()
    }
}