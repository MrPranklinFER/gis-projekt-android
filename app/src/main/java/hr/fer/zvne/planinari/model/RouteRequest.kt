package hr.fer.zvne.planinari.model

class RouteRequest private constructor(val points: List<List<Double>>) {
    companion object {
        fun fromNavigationPoints(points: List<NavigationPoint>) : RouteRequest {
            return RouteRequest(points.map { arrayListOf(it.lon, it.lat) })
        }
    }

    override fun toString(): String {
        return "RouteRequest(points=$points)"
    }
}
