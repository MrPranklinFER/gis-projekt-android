# Planinari

## Mapbox Setup

### 1. add your public access token as a string resource named `mapbox_access_token`

**Exmaple:**
create a file: `app/src/main/res/values/mapbox.xml` with the following content
```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="mapbox_access_token">your public access token</string>
</resources>
```

### 2. add your private access token to `gradle.properties`
add the following line to `gradle.properties`
```
MAPBOX_DOWNLOADS_TOKEN=<your secret token>
```

this variable is then further used inside `settings.gradle` file

### Notes:
- both tokens can be found on your account page: [https://account.mapbox.com/access-tokens](https://account.mapbox.com/access-tokens)
- public access token must be named exactly `mapbox_access_token`
- secret token must have downloads:read scope granted during creation
